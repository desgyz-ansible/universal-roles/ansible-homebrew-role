#!/bin/bash
if [ "$TRAVIS_OS_NAME" == "osx" ]; then
  URL=https://raw.githubusercontent.com/Homebrew/install/master/uninstall
  curl -sLO "${URL}"
  chmod +x ./uninstall
  ./uninstall --force
  sudo rm -rf /usr/local/Homebrew
  sudo rm -rf /usr/local/Caskroom
  sudo rm -rf /usr/local/bin/brew
  ansible --version
  molecule test
fi