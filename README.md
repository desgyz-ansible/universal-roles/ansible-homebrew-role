# Ansible Role: Homebrew

[![Travis](https://img.shields.io/travis/desgyz/ansible-homebrew-role.svg?style=flat-square)](https://travis-ci.org/desgyz/ansible-homebrew-role)
[![MIT licensed][mit-badge]][mit-link]
[![Ansible Role](https://img.shields.io/ansible/role/d/27394.svg?style=flat-square)](https://galaxy.ansible.com/desgyz/ansible_homebrew_role)

Installs [Homebrew][homebrew] on MacOS, and configures packages, taps, services and cask apps according to supplied variables.

This role is an exact copy of [ansible-role-homebrew](https://github.com/geerlingguy/ansible-role-homebrew) by [Jeff Geerling](https://github.com/geerlingguy) the only difference is I merged some of the pending Pull Requests, integrated Molecule with TestInfra for Continous Integration and attempted to fix some pending issues. The purpose of this repository was for me to learn about how to develop Continous Integration for Ansible roles using Molecule and TravisCI while also giving a Role I personally use some TLC.

This code is mirrored from my private GitLab Account. Issues on my GitHub may take a while for me to respond to, this is the same for PRs'.

## Requirements

- [desgyz.ansible_osx_commandline_tools_role][dep-osx-clt-role]

## Role Variables

Available variables are listed below, along with default values (see [`defaults/main.yml`](defaults/main.yml)):

    homebrew_repo: https://github.com/Homebrew/brew

The GitHub repository for Homebrew core.

    homebrew_prefix: /usr/local
    homebrew_install_path: "{{ homebrew_prefix }}/Homebrew"

The path where Homebrew will be installed (`homebrew_prefix` is the parent directory). It is recommended you stick to the default, otherwise Homebrew might have some weird issues. If you change this variable, you should also manually create a symlink back to /usr/local so things work as Homebrew expects.

    homebrew_brew_bin_path: /usr/local/bin

The path where `brew` will be installed.

    homebrew_installed_packages:
      - ssh-copy-id
      - pv
      - { name: vim, install_options: "with-luajit,override-system-vi" }

Packages you would like to make sure are installed via `brew install`. You can optionally add flags to the install by setting an `install_options` property, and if used, you need to explicitly set the `name` for the package as well.

    homebrew_uninstalled_packages: []

Packages you would like to make sure are _uninstalled_.

    homebrew_upgrade_all_packages: no

Whether to upgrade homebrew and all packages installed by homebrew. If you prefer to manually update packages via `brew` commands, leave this set to `no`.

    homebrew_taps:
      - homebrew/cask
      - { name: "koekeishiya/formulae", url: "https://github.com/koekeishiya/homebrew-formulae" }

Taps you would like to make sure Homebrew has tapped, works with URLs.

    homebrew_cask_apps:
      - firefox

Apps you would like to have installed via `cask`. [Search][caskroom] for popular apps to see if they're available for install via Cask. Cask will not be used if it is not included in the list of taps in the `homebrew_taps` variable.

    homebrew_cask_uninstalled_apps:
      - google-chrome

Apps you would like to make sure are _uninstalled_.

    homebrew_cask_appdir: /Applications

Directory where applications installed via `cask` should be installed.

    homebrew_use_brewfile: true

Whether to install via a Brewfile. If so, you will need to install the `homebrew/bundle` tap, which could be done within `homebrew_taps`.

    homebrew_brewfile_dir: '~'

The directory where your Brewfile is located.

    homebrew_clean_package_cache: false

Set to `true` to execute `brew cleanup -s` after intallation of packages.

    homebrew_clean_cask_cache: false

Set to `true` to execute `brew cask cleanup` after installation of casks.

    homebrew_services:
      - mysql

Homebrew services to start at login. Read more at https://github.com/Homebrew/homebrew-services.

## Dependencies

- [desgyz.ansible_osx_commandline_tools_role][dep-osx-clt-role]

## License

[MIT][mit-link]

## Author Information

This role was created in 2014 by [Jeff Geerling][author-website], author of
[Ansible for DevOps][ansible-for-devops] and maintained by [Elliot Weiser](https://github.com/elliotweiser) before I forked it for personal usage.

### Maintainer(s)

- [Audi Bailey (Desgyz)](https://github.com/desgyz)

[ansible-for-devops]: https://www.ansiblefordevops.com/
[author-website]: https://www.jeffgeerling.com/
[caskroom]: https://caskroom.github.io/search
[homebrew]: http://brew.sh/
[mit-badge]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[mit-link]: https://raw.githubusercontent.com/desgyz/ansible-homebrew/master/LICENSE
[dep-osx-clt-role]: https://galaxy.ansible.com/desgyz/ansible_osx_commandline_tools_role