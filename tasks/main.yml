---
# Homebrew setup prerequisites.

- name: Register Home Directory
  set_fact:
    home_dir: "{{ lookup('env', 'HOME') }}"

- name: Ensure Homebrew parent directory has correct permissions (on High Sierra).
  file:
    path: "{{ homebrew_prefix }}"
    owner: root
    state: directory
  become: yes
  when: "ansible_distribution_version is version_compare('10.13', '>=')"

- name: Ensure Homebrew parent directory has correct permissions (<10.13).
  file:
    path: "{{ homebrew_prefix }}"
    owner: root
    group: admin
    state: directory
    mode: 0775
  become: yes
  when: "ansible_distribution_version is version_compare('10.13', '<')"

- name: Ensure Homebrew directory exists.
  file:
    path: "{{ homebrew_install_path }}"
    owner: "{{ ansible_user_id }}"
    group: admin
    state: directory
    mode: 0775
  become: yes

# Clone Homebrew.
- name: Ensure Homebrew is installed.
  git:
    repo: "{{ homebrew_repo }}"
    version: master
    dest: "{{ homebrew_install_path }}"
    update: no
    accept_hostkey: yes
    depth: 1

# Adjust Homebrew permissions.
- name: Ensure proper permissions and ownership on homebrew_brew_bin_path dirs.
  file:
    path: "{{ homebrew_brew_bin_path }}"
    state: directory
    owner: "{{ ansible_user_id }}"
    group: admin
    mode: 0775
  become: yes

- name: Ensure proper ownership on homebrew_install_path subdirs.
  file:
    path: "{{ homebrew_install_path }}"
    state: directory
    owner: "{{ ansible_user_id }}"
    group: admin
  become: yes

# Place brew binary in proper location and complete setup.
- name: Check if homebrew binary is already in place.
  stat: "path={{ homebrew_brew_bin_path }}/brew"
  register: homebrew_binary

- name: Symlink brew to homebrew_brew_bin_path.
  file:
    src: "{{ homebrew_install_path }}/bin/brew"
    dest: "{{ homebrew_brew_bin_path }}/brew"
    state: link
  when: homebrew_binary.stat.exists == false
  become: yes

- name: Ensure proper homebrew folders are in place.
  file:
    path: "{{ homebrew_prefix }}/{{ item }}"
    state: directory
    owner: "{{ ansible_user_id }}"
    group: admin
  become: yes
  with_items:
    - Cellar
    - Homebrew
    - Frameworks
    - Caskroom
    - bin
    - etc
    - include
    - lib
    - opt
    - sbin
    - share
    - share/zsh
    - share/zsh/site-functions
    - var

- name: Force update brew after installation.
  command: "{{ homebrew_brew_bin_path }}/brew update --force"
  when: homebrew_binary.stat.exists == false

# Tap URLs.
- name: Ensure configured taps are tapped.
  homebrew_tap:
    name: "homebrew/bundle"
    state: present
  when: homebrew_use_brewfile

- name: Ensure configured taps are tapped.
  homebrew_tap:
    name: "{{ item.name | default(item) }}"
    url: "{{ item.url | default(omit) }}"
    state: present
  with_items: "{{ homebrew_taps }}"

# Cask.
- name: Check for installed Casks
  shell: brew cask list | grep "{{ item }}" | cat
  register: installed_cask_applications
  with_items: "{{ homebrew_cask_uninstalled_apps }}"
  changed_when: False
  ignore_errors: true

- name: Delete Pre-Install Casks
  homebrew: "name={{ item }} state=absent"
  with_items: "{{ homebrew_cask_uninstalled_apps }}"
  when: item in installed_cask_applications.results|map(attribute='stdout')

- name: Install configured cask applications.
  homebrew_cask:
    name: "{{ item }}"
    state: present
    install_options: "appdir={{ homebrew_cask_appdir }}"
  with_items: "{{ homebrew_cask_apps }}"
  notify:
    - Clear cask cache

- name: Ensure blacklisted cask applications are not installed.
  homebrew_cask: "name={{ item }} state=absent"
  with_items: "{{ homebrew_cask_uninstalled_apps }}"

# Brew.
- name: Check for installed apps
  shell: brew list | grep "{{ item }}" | cat
  register: installed_applications
  with_items: "{{ homebrew_uninstalled_packages }}"
  changed_when: False
  ignore_errors: true

- name: Delete Pre-Install Applications
  homebrew: "name={{ item }} state=absent"
  with_items: "{{ homebrew_uninstalled_packages }}"
  when: item in installed_applications.results|map(attribute='stdout')

- name: Ensure configured homebrew packages are installed.
  homebrew:
    name: "{{ item.name | default(item) }}"
    install_options: "{{ item.install_options | default(omit) }}"
    state: present
  with_items: "{{ homebrew_installed_packages }}"
  notify:
    - Clear package cache

- name: Ensure blacklisted homebrew packages are not installed.
  homebrew: "name={{ item }} state=absent"
  with_items: "{{ homebrew_uninstalled_packages }}"

- name: Upgrade all homebrew packages (if configured).
  homebrew: update_homebrew=yes upgrade_all=yes
  when: homebrew_upgrade_all_packages
  notify:
    - Clear package cache

- name: Check for Brewfile.
  stat:
    path: "{{ homebrew_brewfile_dir }}/Brewfile"
  register: homebrew_brewfile

- name: Check if Brewfile contains updates
  command: brew bundle check --file="{{ homebrew_brewfile_dir }}/Brewfile"
  register: bundle_check_result
  changed_when: bundle_check_result.rc == 1
  when: homebrew_brewfile.stat.exists and homebrew_use_brewfile
  ignore_errors: true

- name: Install apps using Brewfile
  command: brew bundle install --file="{{ homebrew_brewfile_dir }}/Brewfile"
  when: bundle_check_result.rc == 1
  changed_when: False
  ignore_errors: true

# Brew Services
- name: Check for Service Started.
  stat:
    path: "{{ home_dir }}/Library/LaunchAgentshomebrew.mxcl.{{ item }}.plist"
  register: homebrew_services_started
  with_items: "{{ homebrew_services }}"

- name: Ensure configured homebrew services are started at login.
  command: "brew services start {{ item.0 }}"
  register: brew_start
  when: not item.1.stat.exists
  changed_when: 'brew_start.stdout == "==> Successfully started `item.0` (label: homebrew.mxcl.item.0)"'
  with_together:
    - "{{ homebrew_services }}"
    - "{{ homebrew_services_started.results }}"
